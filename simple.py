import time
from http.server import HTTPServer
from http.server import SimpleHTTPRequestHandler


def app(environ, start_response):
    start_response('200 OK', [('Content-type', 'text/plain')])
    return 'test response'


class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        """Serve a GET request."""
        print(self.command)
        print(self.headers)
        time.sleep(float(self.headers.get('wait', 0)))
        return super().do_GET()


if __name__ == '__main__':
    s = HTTPServer(('0.0.0.0', 1025), MyHandler)
    s.serve_forever()
